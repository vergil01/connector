package uk.ac.leedsBeckett

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Shared

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AccountManagementService)
class AccountManagementServiceSpec extends Specification {
	@Shared
    def testAccount

    def setup() {
        testAccount = [id: 1, number: 123456, balance: 1000.00, pin: '1234']
        service.updateAccount(1, 1000.00)
    }

    void "test withdrawal returns true and adjusts the balance when there are enough funds"() {
        when: "the service's withdraw() method is called"
        def result = service.withdraw(testAccount.id, 5.50D)
        then: "the result is true and the balance is adjusted"
        result
        service.getBalance(testAccount.id) == 994.50
    }
    
    void "test get the account balance"() {
        when: "the service method is called"
        def result = service.getBalance(testAccount.id)

        then:"the correct result is returned"
        result == 1000.00
    }

    void "test method to check that the PIN is correct"() {
        when: "the service method is called"
        def result = service.checkPin(testAccount.id, testAccount.pin)

        then:"the correct result is returned"
        result
    }
}
