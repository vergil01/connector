package uk.ac.leedsBeckett

import grails.transaction.Transactional
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.ContentType.URLENC
import static groovyx.net.http.Method.GET
import static groovyx.net.http.Method.PUT
import static groovyx.net.http.ContentType.JSON

@Transactional
class AccountManagementService {

    private def getAccount(Integer accountId) {
        def http = new HTTPBuilder('http://localhost:8080')
        http.request(GET, JSON) {
            uri.path = '/account/' + accountId
            response.success = { res, json ->
                return json
            }
        }
    }

    private boolean updateAccount(Integer accountId, Double balance) {
        boolean result = false
        def http = new HTTPBuilder('http://localhost:8080')
        http.request(PUT) {
            uri.path = '/account/' + accountId
            requestContentType = URLENC
            body = [balance: balance.toString()]
            response.success = {res ->
                result = res.statusLine.statusCode == 200
            }
        }
        return result
    }

    Double getBalance(Integer accountId) {
        def account = getAccount(accountId)
        return account?.balance
    }

    Boolean checkPin(Integer accountId, String pin) {
        def account = getAccount(accountId)
        return account?.pin == pin
    }

    Boolean withdraw(Integer accountId, Double amount) {
        def result = false
        def account = getAccount(accountId)
        if (account.balance >= amount) {
            result = updateAccount(accountId, account.balance - amount)
        }
        return result
    }
}
